window.permissions = { 
    "METADATA_IMPORT": [ "CanImportStructures"],
    "METADATA_BROWSE": [ "CanReadStructuralMetadata"],    
    "ADMIN": [ "CanModifyStoreSettings"],
    "DATA_DATASET": ["CanPerformInternalMappingConfig"],
    "MAPPING": ["CanPerformInternalMappingConfig"],
    "DATAFLOW_WIZARD": ["CanImportStructures", "CanReadStructuralMetadata", "CanPerformInternalMappingConfig"]
  }